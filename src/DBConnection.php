<?php

class DBConnection
{
	
	private $host;
	private $port;
	private $dbname;
	private $user;
	private $pass;
	
	public function __construct(string $host, string $port, string $dbname,
			string $user, string $pass)
	{
		$this->host = $host;
		$this->port = $port;
		$this->dbname = $dbname;
		$this->user = $user;
		$this->pass = $pass;
	}
	
	public function connect()
	{
		$conexion = null;

        try
        {
			$conexion = new PDO(
					"mysql:host={$this->host};"
					. "port={$this->port};"
					. "dbname={$this->dbname};"
					. "charset=utf8;",
					 "{$this->user}","{$this->pass}");
        }
        catch (PDOException $e)
        {
                //var_dump($e);
        }

		return $conexion;
	}
	
}
