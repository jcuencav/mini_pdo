<?php

require '../src/DBConnection.php';
require '../src/config.php';

$rowDatos = [];
$db = new DBConnection($host, $port, $dbname, $user, $pass);

$conn = $db->connect();

$statement = 'SELECT id, name, email, password FROM user';
$consulta = $conn->prepare($statement);

if ($consulta->execute())
{
	while($row = $consulta->fetch())
	{
		$rowDatos []= $row;

	}
}



?>
<!DOCTYPE html>
  <html>
    <head>
		<title>DBConnection - Clientes</title>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>

		<div class="container">
			<div>Users</div>
			<div class="row">
				<?php foreach($rowDatos as $row) : ?>
				<div class="row">
					<div class="col s3"><?php echo $row['id']; ?></div>
					<div class="col s3"><?php echo $row['name']; ?></div>
					<div class="col s3"><?php echo $row['email']; ?></div>
					<div class="col s3"><?php echo $row['password']; ?></div>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
		
      <!--JavaScript at end of body for optimized loading-->
      <script type="text/javascript" src="js/materialize.min.js"></script>
    </body>
  </html>